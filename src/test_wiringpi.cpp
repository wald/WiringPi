#include <wiringPi.h>
#include <ctime>
#include <iostream>
#include <thread>
#include <signal.h>
#include <string>
#include <cstring>
#include <vector>
#include <cmath>
#include <pigpio.h>

using namespace std;

int main(int argc, char ** argv){

  bool verbose = false;
  uint32_t pin = 0;
  bool action = false;
  
  //Parse arguments
  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-h");
    if(pos==NULL) continue;
    cout << "Usage: " << argv[0] << " [-l led] [-a action] [-v]" << endl
	 << "-p pin   : pin to control (0..40)" << endl
	 << "-a action: 0=turn off, 1=turn on" << endl
	 << "-v       : enable verbose mode" << endl;

    return 0;
  }

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-p");
    if(pos==NULL) continue;
    if(strlen(argv[i])>2){
      if(strstr(&argv[i][2],"0x")){pin = stoi(&argv[i][2],NULL,16);}
      else{pin = atoi(&argv[i][2]);}
    }else{
      if(strstr(argv[i+1],"0x")){pin = stoi(argv[i+1],NULL,16);}
      else{pin = atoi(argv[i+1]);}
    }
  }

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-a");
    if(pos==NULL) continue;
    if(strlen(argv[i])>2){
      if(strstr(&argv[i][2],"0x")){action = stoi(&argv[i][2],NULL,16);}
      else{action = atoi(&argv[i][2]);}
    }else{
      if(strstr(argv[i+1],"0x")){action = stoi(argv[i+1],NULL,16);}
      else{action = atoi(argv[i+1]);}
    }
  }
  
  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-v");
    if(pos==NULL) continue;
    verbose=true;
  }

  cout << "pin: " << pin << endl
       << "action: " << action << endl
       << "verbose: " << verbose << endl;


  wiringPiSetup();

  pinMode (pin, OUTPUT);
  
  cout << "Pin is: " << digitalRead(pin) << endl;

  digitalWrite(pin,action);

  cout << "Pin is: " << digitalRead(pin) << endl;
  
  cout << "Have a nice day" << endl;
  return 0;
}

